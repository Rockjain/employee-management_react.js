var express = require('express');
var router = express.Router();
var md5 = require('md5');
var Request = require("request");
const sequelize = require("../models/User")
const jwt = require("jsonwebtoken")
var PHPUnserialize = require('php-unserialize');
var phpUnserialize = require('phpunserialize');
var serialize = require('node-serialize');
const Serialize = require('php-serialize')
/* GET users listing. */

process.env.SECRET_KEY = 'secret'

router.get('/', function(req, res, next) {
  res.send('respond with a resource');
}); 

router.get('/admin',function(req,res){
  sequelize.query("SELECT id, username, ragistered, role, status FROM admin",{type: sequelize.QueryTypes.SELECT, raw: true}).then(rows=>{
    res.json(rows);
    console.log(rows);
  }) 
})

router.post('/login',function(req,res){
  let username = req.body.username;
  sequelize.query("SELECT * FROM admin WHERE username=? and password =?",{type:sequelize.QueryTypes.SELECT, raw:true,  replacements: [
    username, md5(req.body.password)
  ]}).then(rows => {
    // console.log(rows)
     // authId = rows[0].auth_id;
    
      if (rows.length == 0) {
        res.json({
          success:false,
          message:'UserId not exist'
        });
      } else {
        let id = rows[0].id;
        let token = jwt.sign({username: username, id:id}, process.env.SECRET_KEY, {
          expiresIn: '24h'
        })
        res.json({
          success: true,
          message: 'Authentication successful!',
          token: token,
          rows
        });
      }
    })
})

const checkToken = (req, res, next) => {
  const header = req.headers['authorization'];

  if (header !== 'undefined') {
    const token = header;
    req.token = token;
    next();
  } else {
    res.sendStatus(403)
  }
}

router.get('/profile',checkToken,function(req,res){
  let token = jwt.verify(req.token, process.env.SECRET_KEY)

  sequelize.query("SELECT * FROM admin WHERE id = ?",{type:sequelize.QueryTypes.SELECT, raw:true,  replacements: [token.id]}).then(rows=>{
    console.log(rows)
    res.json(rows)
  })
})


router.post('/editprofile',function(req,res){
  sequelize.query("UPDATE admin SET password =? WHERE id=? and password = ?",{type:sequelize.QueryTypes,raw:true,replacements:[md5(req.body.newpassword),req.body.id,md5(req.body.password)]}).then(rows=>{
    console.log(rows)
    res.json(rows)
  })
})

router.post('/addemployee',function(req,res){
  let updated_date = Date.now();
  let accountinfo ={
    account_name:req.body.account_name,
    account_number:req.body.account_number,
    banck_name:req.body.bank_name,
    branch_name:req.body.branch_name,
    ifsccode:req.body.ifsccode
  }

  let account_info =  Serialize.serialize(accountinfo)
  let preivious_experience = Serialize.serialize(req.body.preivious_experience);
  console.log("work",account_info,preivious_experience);
 // console.log("work",PHPUnserialize.unserialize(account_info)); 
 // console.log(req.body.parmanent_address_l1,req.body.parmanent_address_l2,req.body.parmanent_city,req.body.parmanent_state,req.body.parmanent_country,req.body.parmanent_pincode,req.body.local_address_l1,req.body.local_address_l2,req.body.local_city,req.body.local_state,req.body.local_country,req.body.local_pincode) 
  
  //console.log("work",PHPUnserialize.unserialize(datas));

  sequelize.query("INSERT INTO employe (name,father_name,dob,gender,parmanent_address_l1,parmanent_address_l2,parmanent_city,parmanent_state,parmanent_country,parmanent_pincode,local_address_l1,local_address_l2,local_city,local_state,local_country,local_pincode,pan_card_no,adhar_card_no,emp_code,joining_date,preivious_experience,updated_date,department,account_info,status) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);",
  {type:sequelize.QueryTypes.INSERT,raw:true,replacements:[
    req.body.name,req.body.father_name,req.body.dob,req.body.gender,req.body.parmanent_address_l1,req.body.parmanent_address_l2,req.body.parmanent_city,req.body.parmanent_state,req.body.parmanent_country,req.body.parmanent_pincode,req.body.local_address_l1,req.body.local_address_l2,req.body.local_city,req.body.local_state,req.body.local_country,req.body.local_pincode,req.body.pan_card,req.body.adhar_card,req.body.emp_code,req.body.joining_date,preivious_experience,updated_date,req.body.department,
    account_info,req.body.status
  ]}).then(rows=>{
    res.json(rows)
  })
})

router.get('/getemployee',function(req,res){
  sequelize.query("SELECT * FROM employe WHERE id = ?",{type:sequelize.QueryTypes.SELECT,raw:true,replacements:["4"]}).then(rows=>{
    console.log(rows)
    let account = phpUnserialize(rows[0].account_info)
    console.log("account",account)
  })
})
module.exports = router;
