/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from 'react';
import { Link, NavLink } from 'react-router-dom';
import { Badge, UncontrolledDropdown, DropdownItem, DropdownMenu, DropdownToggle, Nav, NavItem } from 'reactstrap';
import PropTypes from 'prop-types';

import { AppAsideToggler, AppNavbarBrand, AppSidebarToggler } from '@coreui/react';
//import logo from '../../assets/img/brand/logo.svg'

import logo from '../../assets/images/download.png'

const propTypes = {
  children: PropTypes.node,
};

const defaultProps = {};

class DefaultHeader extends Component { 
  render() {

    // eslint-disable-next-line
    const { children, ...attributes } = this.props;

    var log;
    var logfunction;
    var profilefunction;
    if(localStorage.getItem("token")){
      log = "Logout";
      logfunction = e => this.props.onLogout(e);
      profilefunction = e=>this.props.profile(e);
    }else{
      log = "Login";
      logfunction = e => this.props.onLogin(e);
      profilefunction=""
    }

    return (
      <React.Fragment>
        <AppSidebarToggler className="d-lg-none" display="md" mobile />
        <h4 className="mx-4 my-2"><img src={logo} alt="" style={{width:"30px",height:"40px"}}/> FOREBEAR</h4>
        <AppSidebarToggler className="d-md-down-none" display="lg" />

        <Nav className="d-md-down-none" navbar>
          <NavItem className="px-3">
            <NavLink to="/dashboard" className="nav-link" >Dashboard</NavLink>
          </NavItem>
          <NavItem className="px-3">
            <Link to="/users" className="nav-link">Users</Link>
          </NavItem>
        
        </Nav>
        <Nav className="ml-auto" navbar>
         
          <UncontrolledDropdown nav direction="down">
            <DropdownToggle nav>
              <img src={'../../assets/img/avatars/1.jpg'} className="img-avatar" alt="admin@bootstrapmaster.com" />
            </DropdownToggle>
            <DropdownMenu right>
             
              <DropdownItem onClick={profilefunction}><i className="fa fa-user" ></i> Profile</DropdownItem>
              <DropdownItem><i className="fa fa-wrench"></i> Settings</DropdownItem>
              
              
              <DropdownItem onClick={logfunction}><i className="fa fa-lock"></i>{log} </DropdownItem>
            </DropdownMenu>
          </UncontrolledDropdown>
        </Nav>
        <AppAsideToggler className="d-md-down-none" />
        {/*<AppAsideToggler className="d-lg-none" mobile />*/}
      </React.Fragment>
    );
  }
}

DefaultHeader.propTypes = propTypes;
DefaultHeader.defaultProps = defaultProps;

export default DefaultHeader;
