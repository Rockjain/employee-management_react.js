export default {
  items: [
    {
      name: 'Dashboard',
      url: '/dashboard',
      icon: 'icon-speedometer',

    },
      {
      name: 'Add Employee',
      url: '/addemployee',
      icon: 'icon-speedometer',
    },
    {
      name: 'Profile',
      url: '/profile',
      icon: 'icon-speedometer'
    },
    {
      divider: true,
    },
 
  ],
};
