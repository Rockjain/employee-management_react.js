import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Badge, Card, CardBody, CardHeader, Col, Row, Table } from 'reactstrap';
import axios from 'axios'
import Moment from 'react-moment';
//import usersData from './UsersData'

function UserRow(props) {
  const user = props.user
  const userLink = `/users/${user.id}`

  const getBadge = (status) => {
    return status === 'Active' ? 'success' :
      status === 'Inactive' ? 'secondary' :
        status === 'Pending' ? 'warning' :
          status === 'Banned' ? 'danger' :
            'primary'
  }

 const Capitalize=(str)=>{
    return str.charAt(0).toUpperCase() + str.slice(1);
    }
    
  return (
    <tr key={user.id.toString()}>
      <th scope="row"><Link to={userLink}>{user.id}</Link></th>
      <td><Link to={userLink}>{Capitalize(user.username)}</Link></td>
      <td><Moment format="YYYY/MM/DD">{user.ragistered}</Moment></td>
      <td>{Capitalize(user.role)}</td>
      <td><Link to={userLink}><Badge color={getBadge(Capitalize(user.status))}>{Capitalize(user.status)}</Badge></Link></td>
    </tr>
  )
}

class Users extends Component {
  constructor(props){
    super(props);
    this.state = ({admin:[]});
  }

  componentDidMount () {
      axios.get("http://localhost:3002/users/admin").then(res=>{
        console.log("data",res.data)
        this.setState({admin:res.data})
        console.log(this.state.admin)
      })
  }

 
  render() {

   const userList = this.state.admin.filter((user) => user.id < 10)

    return (
      <div className="animated fadeIn">
        <Row>
          <Col xl={6}>
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify"></i> Users <small className="text-muted">example</small>
              </CardHeader>
              <CardBody>
                <Table responsive hover>
                  <thead>
                    <tr>
                      <th scope="col">id</th>
                      <th scope="col">name</th>
                      <th scope="col">registered</th>
                      <th scope="col">role</th>
                      <th scope="col">status</th>
                    </tr>
                  </thead>
                  <tbody>
                  {userList.map((user, index) =>
                      <UserRow key={index} user={user}/>
                    )}
                  </tbody>
                </Table>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    )
  }
}

export default Users;
