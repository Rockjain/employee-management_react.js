/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from 'react';

import { Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';
import axios from 'axios';
import {Redirect} from 'react-router-dom';


class Login extends Component {
  constructor(props){
    super(props);
    this.state = ({data:[],username:"",password:""})
  }

  
  componentDidMount(){
     
  }

  login=(event)=>{
   // alert("you are submitting "+this.state.username+" "+this.state.password)
   event.preventDefault();
    axios.post("http://localhost:3002/users/login",{username:this.state.username,password:this.state.password}).then(res=>{
      console.log(res.data);
     
      localStorage.setItem("token",res.data.token)
      this.props.history.push('/dashboard')
    })

  }
  
  myChangeHandler = (event) => {
    let nam = event.target.name;
    let val = event.target.value;
    this.setState({[nam]: val});
  }

  render() {
    if(localStorage.getItem("token")){
      return <Redirect to="/dashboard" />
    }else{
    return (
      <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="8">
              <CardGroup>
                <Card className="p-4">
                  <CardBody>
                    <Form onSubmit={this.login}>
                      <h1>Login</h1>
                      <p className="text-muted">Sign In to your account</p>
                      <InputGroup className="mb-3">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-user"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input type="text" placeholder="Username" name="username" onChange={this.myChangeHandler} />
                      </InputGroup>
                      <InputGroup className="mb-4">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-lock"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input type="password" placeholder="Password" name="password" onChange={this.myChangeHandler} />
                      </InputGroup>
                      <Row>
                        <Col xs="6">
                          <Button color="primary" className="px-4" type="submit">Login</Button>
                        </Col>
                        <Col xs="6" className="text-right">
                          <a color="link" className="px-0">Forgot password?</a>
                        </Col>
                      </Row>
                    </Form>
                  </CardBody>
                </Card>
                
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
  }
}

export default Login;
