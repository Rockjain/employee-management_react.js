/* eslint-disable jsx-a11y/anchor-is-valid */
import React, {Component} from 'react';
import {Badge, Col, Nav, NavItem, NavLink, Row, TabContent, TabPane, Card, CardBody, CardHeader,Button} from 'reactstrap';
import classnames from 'classnames';
import serialize from 'form-serialize';
import $ from 'jquery';
import axios from 'axios';

class AddEmployee extends Component{

  
    constructor(props) {
        super(props);
    
        this.toggle = this.toggle.bind(this);
        this.state = {
          activeTab: new Array(5).fill('1'),checked:false,name:"",fathername:"",dob:"",gender:"",pancard:"",adharcard:"",
          parmanentaddress1:"",parmanentaddress2:"",parmanentcity:"",parmanentstate:"",parmanentcountry:"",parmanentpincode:"",
          localaddress1:"",localaddress2:"",localcity:"",localstate:"",localcountry:"",localpincode:"",status:"",salary:"",releivingdate:"",workexprience:"",empcode:"",joiningdate:"",department:"",accountname:"",accountnumber:"",
          bankname:"",branchname:"",ifsccode:"",preivious_experience:[{companyname:"",joiningdate:"",releivingdate:"",designation:""}]
        };
        this.sameaddress = this.sameaddress.bind(this);
      
      }
     
      sameaddress() {
        this.setState({
          checked: !this.state.checked
        })
      }

      toggle(tabPane, tab) {
        const newArray = this.state.activeTab.slice()
        newArray[tabPane] = tab
        this.setState({
          activeTab: newArray,
        });
      }

      myChangeHandler = (event) => {
       
          let nam = event.target.name;
          let val = event.target.value;
          this.setState({[nam]: val});
        
        
      }


      addWorkField(){
        this.setState((prevState)=>({preivious_experience:[...prevState.preivious_experience,{companyname:"",joiningdate:"",releivingdate:"",designation:""}]}))
      }

      handlecompanyChange = idx => evt => {
        const newexprience = this.state.preivious_experience.map((exprience, sidx) => {
          if (idx !== sidx) return exprience;
          return { ...exprience, companyname: evt.target.value};
        });
         this.setState({ preivious_experience: newexprience });
         
      }

      handlejoiningChange = idx => evt => {
        const newexprience = this.state.preivious_experience.map((exprience, sidx) => {
          if (idx !== sidx) return exprience;
          return { ...exprience,joiningdate: evt.target.value};
        });
         this.setState({ preivious_experience: newexprience });
      
      }

      handlereleivingChange = idx => evt => {
        const newexprience = this.state.preivious_experience.map((exprience, sidx) => {
          if (idx !== sidx) return exprience;
          return { ...exprience, releivingdate: evt.target.value};
        });
         this.setState({ preivious_experience: newexprience });
        
      }

      handledesignationChange = idx => evt => {
        const newexprience = this.state.preivious_experience.map((exprience, sidx) => {
          if (idx !== sidx) return exprience;
          return { ...exprience,designation: evt.target.value};
        });
         this.setState({ preivious_experience: newexprience });
        
      }

      commonValidate=()=>{
        //you could do something here that does general validation for any form field
        return true;
      }

      
      addEmploye=(event)=>{
        
        event.preventDefault();
        alert(this.state.name)

        axios.get("http://localhost:3002/users/getemployee")

        axios.post("http://localhost:3002/users/addemployee",{
          name:this.state.name,
          father_name:this.state.fathername,
          dob:this.state.dob,
          gender:this.state.gender,
          parmanent_address_l1:this.state.parmanentaddress1,
          parmanent_address_l2:this.state.parmanentaddress2,
          parmanent_city:this.state.parmanentcity,
          parmanent_state:this.state.parmanentstate,
          parmanent_country:this.state.parmanentcountry,
          parmanent_pincode:this.state.parmanentpincode,
          local_address_l1:this.state.localaddress1,
          local_address_l2:this.state.localaddress2,
          local_city:this.state.localcity,
          local_state:this.state.localstate,
          local_country:this.state.localcountry,
          local_pincode:this.state.localpincode,
          pan_card:this.state.pancard,
          adhar_card:this.state.adharcard,
          emp_code:this.state.empcode,
          joining_date:this.state.joiningdate,
          preivious_experience:this.state.preivious_experience,
          department:this.state.department,
          account_name:this.state.accountname,
          account_number:this.state.accountnumber,
          bank_name:this.state.bankname,
          branch_name:this.state.branchname,
          ifsccode:this.state.ifsccode,
          status:this.state.status,

        }).then(res=>{
          console.log(res.data);
        })
       
        console.log("work",this.state.preivious_experience);
      }

    tabPane() {
     
        return (
          <>
            <TabPane tabId="1">
            <form id="personalinfo">
            <fieldset>
                <legend><b>Personal Information:</b></legend>
            <table className="w-100">
              <tbody>
                
                <tr >
                    <th>Full Name<b><span className="text-danger">*</span></b> :</th>
                    <td>
                      <input type="text" placeholder="Enter Your Full Name" name="name" className="w-50 my-1" onChange={this.myChangeHandler}   required />
                      </td>
                  </tr>

                  <tr >
                    <th>Father's Name<b><span className="text-danger">*</span></b> :</th><td><input type="text" placeholder="Enter Your Father Name" name="fathername" className="w-50 my-1" onChange={this.myChangeHandler} required /></td>
                  </tr>

                  <tr >
                    <th>Date Of Birth :</th><td><input type="date" name="dob" className="w-50 my-1" onChange={this.myChangeHandler} required /></td>
                  </tr>

                  <tr >
                    <th>Gender<b><span className="text-danger">*</span></b> :</th><td>
                      <input type="radio" name="gender" value="male"  onChange={this.myChangeHandler} /> Male
                      <input type="radio" name="gender" value="female" className="ml-3" onChange={this.myChangeHandler} /> Female
                      <input type="radio" name="gender" value="other" className="ml-3" onChange={this.myChangeHandler} /> Other </td>
                  </tr>

                  <tr >
                    <th >Pancard Number<b><span className="text-danger">*</span></b> :</th><td><input type="text" placeholder="Pancard Number" name="pancard" className="w-50 my-1" onChange={this.myChangeHandler} required /></td>
                  </tr>

                  <tr className="my-3">
                    <th >Adharcard Number<b><span className="text-danger">*</span></b> :</th><td><input type="text" placeholder="Adharcard Number" name="adharcard" className="w-50 my-1" onChange={this.myChangeHandler} required /></td>
                  </tr>
                 
                 
                </tbody>
              </table>
              </fieldset>

              </form>

              <form id="adressdetail">
            <fieldset>
                <legend><b>Address Details:</b></legend>
                <form id="localaddress">
            <table className="w-100">
              <tbody>
              <h5>Local Address</h5>
                <tr >
                    <th >Address Line 1:</th><td><input type="text" placeholder="Address Line 1" name="localaddress1" className="w-50 my-1" onChange={this.myChangeHandler}/></td>
                  </tr>

                  <tr >
                    <th >Address Line 2:</th><td><input type="text" placeholder="Address Line 2" name="localaddress2" className="w-50 my-1" onChange={this.myChangeHandler}/></td>
                  </tr>

                  <tr >
                    <th>City :</th><td><input type="text" placeholder="City" name="localcity" className="w-50 my-1" onChange={this.myChangeHandler}/></td>
                  </tr>

                  <tr >
                    <th >State :</th><td><input type="text" placeholder="State" name="localstate" className="w-50 my-1" onChange={this.myChangeHandler}/></td>
                  </tr>

                  <tr className="my-3">
                    <th >Country :</th><td><input type="text" placeholder="Country" name="localcountry" className="w-50 my-1" onChange={this.myChangeHandler}/></td>
                  </tr>
                  
                  <tr className="my-3">
                    <th >Pincode :</th><td><input type="text" placeholder="Pincode" name="localpincode" className="w-50 my-1" onChange={this.myChangeHandler}/></td>
                  </tr>
                  </tbody>
              </table>
                  </form>
                  <input type="checkbox" name="parmanentaddress" value="parmanentaddress" className="my-1" checked={ this.state.checked } 
          onChange={this.sameaddress }/> Please check the box if Parmanent Address is same as the Local Address.<br/>
                
                  <div id="parmanent" hidden={this.state.checked}>
                  <form id="parmanentaddress">
                  <table className="w-100">
                  <tbody>
                    <h5 className="my-3">Parmanent Address</h5>
                    <tr >
                    <th >Address Line 1:</th><td><input type="text" placeholder="Address Line 1" name="parmanentaddress1" className="w-50 my-1" onChange={this.myChangeHandler}/></td>
                  </tr>

                  <tr >
                    <th >Address Line 2:</th><td><input type="text" placeholder="Address Line2" name="parmanentaddress2" className="w-50 my-1" onChange={this.myChangeHandler}/></td>
                  </tr>

                  <tr >
                    <th>City :</th><td><input type="text" placeholder="City" name="parmanentcity" className="w-50 my-1" onChange={this.myChangeHandler}/></td>
                  </tr>

                  <tr >
                    <th >State :</th><td><input type="text" placeholder="State" name="parmanentstate" className="w-50 my-1" onChange={this.myChangeHandler}/></td>
                  </tr>

                  <tr className="my-3">
                    <th >Country :</th><td><input type="text" placeholder="Country" name="parmanentcountry" className="w-50 my-1" onChange={this.myChangeHandler}/></td>
                  </tr>

                  <tr className="my-3">
                    <th >Pincode :</th><td><input type="text" placeholder="Pincode" name="parmanentpincode" className="w-50 my-1" onChange={this.myChangeHandler}/></td>
                  </tr>
                  </tbody>
              </table>
              </form>
                  </div>
                  
              </fieldset>

              </form>
            </TabPane>
            <TabPane tabId="2">

             <form id="workexprience">
             <fieldset>
                <legend><b>Work Experience :</b></legend>
                <div class="field_wrapper">
                <div>
                
                  {this.state.preivious_experience.map((work,index)=>{
                    let companyname= `companyname-${index}` , joiningdate = `joiningdate-${index}`
                    let releivingdate=`releivingdate-${index}` , designation = `designation-${index}`
                   
                    return(
                      <table className="w-100" key={index}>
                      <tbody>
                        <tr className="my-3"  >
                            <th >Company Name :</th><td><input type="text" placeholder="Company name" name={companyname} className="w-50 my-1 compnayname" onChange={this.handlecompanyChange(index)} value={work.companyname}/></td>
                          </tr> 
                        <tr className="my-3" >
                        <th >Joining Date :</th><td><input type="date" name={joiningdate} className="w-50 my-1 joiningdate" onChange={this.handlejoiningChange(index)}/></td>
                      </tr> 
                      <tr className="my-3" >
                        <th >Relieving Date :</th><td><input type="date" name={releivingdate}  className="w-50 my-1 releivingdate" onChange={this.handlereleivingChange(index)}/></td>
                      </tr> 
                      <tr className="my-3" >
                        <th >Designation :</th><td><input type="text" name={designation} placeholder="Designation" className="w-50 my-1 designation" onChange={this.handledesignationChange(index)}/></td>
                      </tr> 
                    </tbody>
                    </table>
                    )
                  })}
                   
                   
                 
                   
                      <a  className="float-right btn btn btn-success add_button" onClick={(e)=>this.addWorkField(e)}>Add</a>
                      </div>
                </div>
                </fieldset>
               </form>

            </TabPane>
            <TabPane tabId="3">
           
            <form id="bankdetail">
             <fieldset>
                <legend><b>Bank details :</b></legend>
                <div>
                  <table className="w-100">
                      <tbody>
                      <tr className="my-3">
                            <th >Account Name :</th><td><input type="text" placeholder="Account Name" name="accountname" className="w-50 my-1" onChange={this.myChangeHandler}/></td>
                          </tr>
                        <tr className="my-3">
                            <th >Account Number :</th><td><input type="text" placeholder="Account Number" name="accountnumber" className="w-50 my-1" onChange={this.myChangeHandler}/></td>
                          </tr>
                          <tr className="my-3">
                            <th >Bank Name :</th><td><input type="text" placeholder="Bank Name" name="bankname" className="w-50 my-1" onChange={this.myChangeHandler}/></td>
                          </tr>
                          <tr className="my-3">
                            <th >Branch Name :</th><td><input type="text" placeholder="Branch Name" name="branchname" className="w-50 my-1" onChange={this.myChangeHandler}/></td>
                          </tr>
                          <tr className="my-3">
                            <th >IFSC Code :</th><td><input type="text" placeholder="IFSC Code" name="ifsccode" className="w-50 my-1" onChange={this.myChangeHandler}/></td>
                          </tr>
                      </tbody>
                    </table>
                </div>
                </fieldset>
               </form>

            </TabPane>
            <TabPane tabId="4">
           
            <form id="extra">
             <fieldset>
                <legend><b>Department/Salary :</b></legend>
                <div>
                  <table className="w-100">
                      <tbody>
                      <tr className="my-3">
                            <th >Department<b><span className="text-danger">*</span></b> :</th><td><input type="text" placeholder="Department" name="department" className="w-50 my-1" onChange={this.myChangeHandler} required/></td>
                          </tr>
                      <tr className="my-3">
                            <th >Salary<b><span className="text-danger">*</span></b> :</th><td><input type="text" placeholder="Salary" name="salary" className="w-50 my-1" onChange={this.myChangeHandler} required /></td>
                          </tr>
                        <tr className="my-3">
                            <th >Status<b><span className="text-danger">*</span></b> :</th><td>
                            <select name="status" id="status" onChange={this.myChangeHandler} className="w-50 my-1">
                                <option >Please Select status of Employee</option>
                                <option value="current_employee">Current Employee</option>
                                <option value="ex_employee">Ex Employee</option>
                                <option value="retired_employee">Retired Employee</option>
                            </select>
                            </td>
                          </tr>
                          <tr className="my-3">
                            <th >Joining Date<b><span className="text-danger">*</span></b> :</th><td><input type="date" name="releivingdate" className="w-50 my-1" onChange={this.myChangeHandler}/></td>
                          </tr>
                          <tr className="my-3">
                            <th >Releiving Date :</th><td><input type="date"  name="joiningdate" className="w-50 my-1" onChange={this.myChangeHandler} /></td>
                          </tr>
                          
                          <tr className="my-3">
                            <th >Employee Code<b><span className="text-danger">*</span></b> :</th><td><input type="text" placeholder="Employee Code" name="empcode" id="empcode" className="w-50 my-1" value={this.state.empcode} onChange={this.myChangeHandler} required="required" /></td>
                          </tr>
                          
                      </tbody>
                    </table>
                </div>
                </fieldset>
                <Button className="mt-2" color="primary">Submit</Button>
               </form>

            
            </TabPane>
          </>
        );
      }

    render(){
      if(localStorage.getItem("token")){
        return (
            <div>
              <Row>
                    <Col>
                        <Card>
                          <CardHeader>
                              <h4> Add Employee </h4>
                          </CardHeader>
                          <CardBody>
                              <Nav tabs>
                                <NavItem>
                                  <NavLink
                                    active={this.state.activeTab[2] === '1'}
                                    onClick={() => { this.toggle(2, '1'); }}
                                  >
                                  <span > Personal Detail</span>
                                  </NavLink>
                                </NavItem>
                                <NavItem>
                                  <NavLink
                                    active={this.state.activeTab[2] === '2'}
                                    onClick={() => { this.toggle(2, '2'); }}
                                  >
                                    <span>Work Experience</span>
                                  </NavLink>
                                </NavItem>
                                <NavItem>
                                  <NavLink
                                    className={classnames({ active: this.state.activeTab[2] === '3' })}
                                    onClick={() => { this.toggle(2,'3'); }}
                                  >
                                    <span>Bank Details</span>
                                  </NavLink>
                                </NavItem>
                                <NavItem>
                                  <NavLink
                                    className={classnames({ active: this.state.activeTab[2] === '4' })}
                                    onClick={() => { this.toggle(2,'4'); }}
                                  >
                                    <span>Department/Status</span>
                                  </NavLink>
                                </NavItem>
                              </Nav>
                              <form name="addemploye" onSubmit={this.addEmploye}>
                              <TabContent activeTab={this.state.activeTab[2]}>
                              
                              {this.tabPane()}
                            
                                
                              </TabContent>
                              </form>
                            
                        </CardBody>
                        </Card>
                    </Col>
                </Row>
            </div>
        )
    }
    else{
      return(
        <div>
           <Row>
                    <Col>
                        <Card>
                          <CardHeader>
                              <h4>Message</h4>
                          </CardHeader>
                          <CardBody>
                              Sorry you are not loggedin.
                            
                        </CardBody>
                        </Card>
                    </Col>
                </Row>
          </div>
      )
    }
  }
}

export default AddEmployee;